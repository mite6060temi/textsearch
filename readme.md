Command-line app that does the following:

Take two files input.txt and patterns.txt:
* input.txt text document composed of 1 or more lines of text, 
* patterns.txt is a set of search strings (1 per line). 

    input.txt

        Hello. This is line 1 of text.
        and this is another.
        line 3 here
        the end

    patterns.txt

        the end
        matches
        line 3
        and this is anoother.


1. Output all the lines from input.txt that match exactly any pattern in patterns.txt
        output:
           the end

2. Output all the lines from input.txt that contain a match from patterns.txt somewhere in the line.
    outputs:
        line 3 here
        the end
3. Output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt
    outputs:
        and this is another.
        the end