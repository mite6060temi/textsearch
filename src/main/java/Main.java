import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Stream;


public class Main {
    public static void main(String[] args) {
        System.out.println("Please specify the path to the input text file:");
        Scanner scanner = new Scanner(System.in);
        String pathToInput = scanner.nextLine();
        System.out.println("Please specify the path to the pattern text file:");
        String pathToPattern = scanner.nextLine();
        scanner.close();

        Supplier<Stream<String>> input = getStreamSupplier(pathToInput);
        Supplier<Stream<String>> pattern = getStreamSupplier(pathToPattern);

        System.out.println("Output all the lines from input.txt that match exactly any pattern in patterns.txt:");

        input.get().forEach(s -> pattern.get().forEach(p -> {
            if (s.equals(p)) System.out.println(s);
        }));

        System.out.println("Output all the lines from input.txt that contain a match from patterns.txt somewhere in the line:");

        input.get().forEach(s -> pattern.get().forEach(p -> {
            if (s.contains(p)) System.out.println(s);
        }));

        System.out.println("Output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt:");

        input.get().forEach(s -> pattern.get().forEach(p -> {
            int dist = StringUtils.getLevenshteinDistance(s, p, 1);
            if (dist >= 0 && dist <= 1) System.out.println(s);
        }));
    }

    private static Supplier<Stream<String>> getStreamSupplier(String path) {
        return () -> {
            try {
                return Files.lines(Paths.get(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Stream.empty();
        };
    }
}

