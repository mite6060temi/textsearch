import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class MainTest {
    private String src = "src/test/resources/input.txt\n" +
            "src/test/resources/patterns.txt";
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private ByteArrayInputStream in = new ByteArrayInputStream(src.getBytes());


    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setIn(in);
    }

    @After
    public void cleanUp() {
        System.setOut(null);
        System.setIn(null);
    }

    @Test
    public void test() {
        Main.main(new String[0]);
        String s = outContent.toString();
        assertEquals(3, StringUtils.countMatches(s, "the end"));
        assertEquals(1, StringUtils.countMatches(s, "line 3 here"));
        assertEquals(1, StringUtils.countMatches(s, "and this is another."));
    }

}
